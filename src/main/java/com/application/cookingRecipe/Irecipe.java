/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.application.cookingRecipe;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;


/**
 *
 * @author anto
 */
public interface Irecipe extends CrudRepository<Recipe, Long> {
Recipe findByName(@Param("name") String name);
Recipe findByNameAndDescription(@Param("name") String name,@Param("description")String description);

}