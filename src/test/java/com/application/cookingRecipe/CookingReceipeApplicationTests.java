package com.application.cookingRecipe;

import java.util.List;
import java.util.Optional;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import static org.junit.Assert.assertTrue;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.annotation.Order;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CookingReceipeApplicationTests {
    
    @Autowired
    private RecipeService service;
    @Autowired
    private Irecipe recipe;
    @Test 
    @Order(1)
        public void testCreate()
        {
            Recipe p = new Recipe("gateau à la myrtille",  "", "oeuf ,lait, myrtille");
            p.setId(1000L);
            recipe.save(p);
            assertNotNull(recipe.findById(1000L).get());
        }

        @Test
           @Order(2)
              public void addIngredient()
        {
          Recipe p = new Recipe("gateau à la myrtille",  "", "oeuf ,lait, myrtille");
            p.setId(1000L);
            recipe.save(p);
    
          
            
           Optional<Recipe> v = recipe.findById(1000L);
           v.get().setIngredient("banane");
           recipe.save(v.get());
           assertEquals("banane",v.get().getIngredient());
        }
               @Test
                 @Order(3)
                        public void supprimerIngredient()
        {
       
           Optional<Recipe> p = recipe.findById(1000L);
           p.get().setIngredient("");
           assertEquals("",p.get().getIngredient());
        }
         @Test
            @Order(5)
        public void testDelete()
        {
            
            recipe.deleteById(1000L);
            
            
            assertEquals(Optional.empty(),recipe.findById(1000L));
        }
        
                 @Test
            @Order(4)
        public void testByName()
        {
             Recipe p1 = new Recipe("gateau à la myrtille",  "", "oeuf ,lait, myrtille");
            p1.setId(1000L);
            recipe.save(p1);
            Recipe p= recipe.findByName("gateau à la myrtille");
            assertEquals("gateau à la myrtille",p.getName());
            
        }
        
                       @Test
            @Order(6)
        public void testsetnameingredientdescription()
        {
           Optional<Recipe> v = recipe.findById(1000L);
           v.get().setIngredient("banane");
           v.get().setName("cake a la banane");
           v.get().setDescription("description banane");
           
           recipe.save(v.get());
           assertEquals("banane",v.get().getIngredient());
            
        }
    
}
     


