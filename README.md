# recipeEpsi
Etudiants : Sonia Marquez - Léonard Antonin 
Classe I1C2

# Les différrentes étapes des stages : 
build_job :  qui compile le projet maven.
test_job :  execute les tests du projet. 
sonarcubecheck : télécharge l'image de maven et execute sonarcube avec les variables défini dans la CICD.


# Suivre les étapes données dans Sonarqube lors de la création d'un projet

# Installer et Configurer GitLab runner
gitlab-runner pour dire qu’on veut utiliser le serveur local donc mon pc au lieu d’un serveur externe.  
Se connecter : docker exec -it gitlab-runner bash  
Configurer :  gitlab-runner register  
Renseigner l'url : Enter the GitLab instance URL (for example, https://gitlab.com/)  
Enter the registration token: "On le trouve dans GitLab, setting CI/CD"  
Enter a description for the runner: ici le nom du pc => pcsonia  
Enter an executor: docker  
Enter the default Docker image : ici => alpine:latest  

La configuration est sauvegardé :   
Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"  
root@47040e44f6ad:/#  

On renseigne dans le runner du gitlab :   
le tags utilisé : ici docker  
le nom : pcsonia  

dans setting ci de gitlab dans la variable SONAR_HOST_URL : renseigner l'IP du pc  

# Configurer le pom.xml
```java
<properties>
	<sonar.qualitygate.wait>true</sonar.qualitygate.wait>  
	<java.version>11</java.version>  
</properties>  
```  







